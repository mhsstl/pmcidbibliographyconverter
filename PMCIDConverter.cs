﻿//Copyright 2017 Washington University

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without 
//limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
//of the Software, and to permit persons to whom the Software is furnished to do so, subject to the 
//following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
//TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
//CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
//DEALINGS IN THE SOFTWARE.

//Washington University in St Louis, Biomedical Engineering Deptartment, Silva Laboratory, Staff Michael Southworth
//michaelsouthworth@wustl.edu

//Uses resources made available by the NIH NLM NCBI and NIH US National Library of Medicine 
//see https://www.ncbi.nlm.nih.gov/pmc/pmctopmid/ for more information

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace edu.wustl.bme.silva.southworth
{
    class PMIDConverter
    {

        static Dictionary<string, string> doiToPMCID = new Dictionary<string, string>();
        static Dictionary<string, string> pmidToPMCID = new Dictionary<string, string>();

        static Func<string, string> PMCIDConverter = (string s) => { return ""; };

        const string NIHBASE = @"https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/";


        /// <summary>
        /// Converts PMID and DOI to PMCID when avaialable, either from CSV or NIH Web API
        /// </summary>
        /// <param name="args">{input filename, output filename, pmcid csv (optional)}</param>
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                System.Console.WriteLine("Usage: [input filename] [output filename] [optional pmcid csv]");
                return;
            }
            string input = args[0];
            string output = args[1];
            Console.WriteLine("Using input: " + input);
            Console.WriteLine("Using output: " + output);

            if (!File.Exists(input))
            {
                Console.WriteLine("Input file not found. " + input);
                return;
            }

            if (args.Length > 2)
            {
                //using csv
                string pmcidCsv = args[2];
                Console.WriteLine("Loading CSV database : " + pmcidCsv);
                LoadPMIDCSV(pmcidCsv);
                PMCIDConverter = GetPMCIDCSV;
                Console.WriteLine("Local CSV database : " + pmcidCsv +" Loaded");
            }
            else
            {
                //using web api
                PMCIDConverter = GetPMCIDWeb;
                Console.WriteLine("NIH Web API : " + NIHBASE);
            }

            if (input.EndsWith(".docx", StringComparison.CurrentCultureIgnoreCase))
            {
                //convert docx
                Console.WriteLine("Converting docx format");
                if (File.Exists(output))
                {
                    Console.WriteLine("Output already exists. Please delete and retry");
                    return;
                }
                ConvertZip(input, output);
            }
            else
            {
                //do plaintext subs
                Console.WriteLine("Converting plaintext format");
                ConvertPlainText(input, output);
            }
            Console.WriteLine("Conversion complete");
        }

        /// <summary>
        /// Does plaintext find and replace for doi: and pmcid: tags with optional spaces
        /// </summary>
        /// <param name="input">input filename</param>
        /// <param name="output">output filename</param>
        static void ConvertPlainText(string input, string output)
        {
            using (var iStream = File.OpenRead(input))
            using (var oStream = File.OpenWrite(output))
            {
                UpdateDocument(iStream, oStream);
            }
        }
        /// <summary>
        /// Copies the content of a zip file to a new document, and does a find and replace on doi: and pmid: tags
        /// Will not catch tags with formatting that changes between characters (i.e. different fonts / sizes for each letter)
        /// </summary>
        /// <param name="input">input docx file</param>
        /// <param name="output">output docx file</param>
        static void ConvertZip(string input, string output)
        {
            using (var inputFile = ZipFile.Open(input, ZipArchiveMode.Read))
            using (var outputFile = ZipFile.Open(output, ZipArchiveMode.Update))
            {
                //copy all the docx entries
                foreach (var entry in inputFile.Entries)
                {
                    var outputEntry = outputFile.CreateEntry(entry.FullName);

                    using (var iStream = entry.Open())
                    using (var oStream = outputEntry.Open())
                    {
                        //one of these xml files has the references. do them all
                        if (entry.FullName.Contains("document.xml"))
                        {
                            //update reference
                            UpdateDocument(iStream, oStream);
                        }
                        else
                        {
                            //copy unmodified
                            iStream.CopyTo(oStream);
                        }

                    }
                }
            }
        }

       
        /// <summary>
        /// Update document with new references
        /// </summary>
        /// <param name="input">input document stream</param>
        /// <param name="output">output document stream</param>
        static void UpdateDocument(System.IO.Stream input, System.IO.Stream output)
        {
            var inputString = new System.IO.StreamReader(input).ReadToEnd();
            inputString = ConvertDOI(inputString);
            inputString = ConvertPMID(inputString);
            using (var sw = new StreamWriter(output))
            {
                sw.Write(inputString);
            }
        }


        /// <summary>
        /// Searches text for doi tags, and updates them with lookup from appropriate pmcid in database
        /// </summary>
        /// <param name="inputDoc">input document in string format</param>
        /// <returns>pmcid string PMC#### or empty string if none</returns>
        static string ConvertDOI(string inputDoc)
        {
            string pattern = @"doi\s?:\s?[\w\.\d/]+";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(inputDoc);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var key = match.Value.Split(':')[1].Trim();
                    var value = PMCIDConverter(key);
                    if (value.Length > 0)
                    {
                        Console.WriteLine("DOI: " + match.Value.Split(':')[1] + " PMCID: " + value);
                        inputDoc = inputDoc.Replace(match.Value, match.Value + " " + value);
                    }
                    else
                    {
                        Console.WriteLine("DOI: " + match.Value.Split(':')[1]); ;
                    }
                }
            }

            return inputDoc;
        }


        /// <summary>
        /// Searches text for pmid tags, and updates them with lookup from appropriate pmcid in database
        /// </summary>
        /// <param name="inputDoc">input document in string format</param>
        /// <returns>pmcid string PMC#### or empty string if none</returns>
        static string ConvertPMID(string inputDoc)
        {
            string pattern = @"pmid\s?:\s?[\d]+";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(inputDoc);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var key = match.Value.Split(':')[1].Trim();
                    var value = PMCIDConverter(key);
                    if (value.Length > 0)
                    {
                        Console.WriteLine("PMID: " + match.Value.Split(':')[1] + " PMCID: " + value);
                        inputDoc = inputDoc.Replace(match.Value, match.Value + " " + value);
                    }
                    else
                    {
                        Console.WriteLine("PMID: " + match.Value.Split(':')[1]); ;
                    }
                }
            }
            return inputDoc;
        }

        /// <summary>
        /// Fetches PMCID from ncbi nih web api
        /// </summary>
        /// <param name="id">doi or pmid to lookup</param>
        /// <returns>pmcid in PMC##### if found</returns>
        private static string GetPMCIDWeb(string id)
        {
            try
            {
                var uri = new Uri(NIHBASE + "?tool=pmcidconverter&email=michaelsouthworth@wustl.edu&ids=" + id);
                HttpClient wc = new HttpClient();
                var response = wc.GetStringAsync(uri).Result;
                if (response.Contains("invalid article id"))
                {
                    return "";
                }

                string pattern = @"version[^>]*pmcid\s*=\s*""([^""]+)""[^>]*current\s*=\s*""true""";
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                MatchCollection matches = rgx.Matches(response);
                if (matches.Count > 0)
                {
                    return matches[0].Groups[1].ToString();
                }
                return "";
            }
            catch (HttpRequestException ex)
            {
                System.Console.WriteLine("Unable to connect to pmcid server: " + ex.Message);
                return "";
            }
        }

        /// <summary>
        /// Perform PMCID lookup from local CSV database
        /// </summary>
        /// <param name="id">id to lookup</param>
        /// <returns>Corresponding PMCID if found</returns>
        private static string GetPMCIDCSV(string id)
        {
            if (doiToPMCID.ContainsKey(id))
            {
                return doiToPMCID[id];
            }
            if (pmidToPMCID.ContainsKey(id))
            {
                return pmidToPMCID[id];
            }
            return "";
        }

        /// <summary>
        /// Load a pmcid database from CSV. Takes a lot of memory at the moment.
        /// </summary>
        /// <param name="pmidFile">CSV file</param>
        static void LoadPMIDCSV(string pmidFile)
        {
            var lines = File.ReadAllLines(pmidFile);
            if (lines.Length == 0)
            {
                System.Console.WriteLine("Invalid CSV File.  No Lines found");
                throw new ArgumentException("Invalid CSV File");
            }
            var headers = lines[0].Split(',');
            var doiIndex = Array.FindIndex(headers, (string s) => { return s.Equals("doi", StringComparison.CurrentCultureIgnoreCase); });
            var pmidIndex = Array.FindIndex(headers, (string s) => { return s.Equals("pmid", StringComparison.CurrentCultureIgnoreCase); });
            var pmcidIndex = Array.FindIndex(headers, (string s) => { return s.Equals("pmcid", StringComparison.CurrentCultureIgnoreCase); });
            if (doiIndex == -1 || pmidIndex == -1 || pmcidIndex == -1)
            {
                System.Console.WriteLine("Invalid CSV File.  No headers found " + headers);
                throw new ArgumentException("Invalid CSV File");
            }

            foreach (var line in lines.Skip(1))
            {
                var fields = line.Split(',');
                if (fields.Length >= 12)
                    if (fields[doiIndex].Length > 0)
                    {
                        doiToPMCID[fields[doiIndex]] = fields[pmcidIndex];
                    }
                if (fields[pmidIndex].Length > 0)
                {
                    pmidToPMCID[fields[pmidIndex]] = fields[pmcidIndex];
                }
            }
        }
    }
}