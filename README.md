# PMCID Bibliography Converter 
A tool to convert DOIs and PMIDs to PMCID in bibliographies.

This project was created to assist in complying with NIH guidance to include PMCID in citations. For more information on the requirement, see https://publicaccess.nih.gov/include-pmcid-citations.htm

This tool is distributed under the MIT License.

The feature is, as of writing, lacking in Mendeley, so this tool was created to look up PMCID based on DOI or PMID, which is available in Mendeley.  Although this tool was written to fill a gap in Mendeley, Mendeley is not a specific requirement in order to be useful.  The tool can perform a lookup on any docx or ascii plain text file with DOIs or PMIDs already included.  Rather than trying to hack your Mendeley database, this takes an existing bibliography and updates it to include PMCIDs to be compliant with submission requirements.

This project was written in C# and is based on .NET Core, so it is in theory compatible with any .NET supported platform.  Binaries are only provided for Windows.

To use with Mendeley, [modify your citation style](https://blog.mendeley.com/2011/05/03/howto-edit-citation-styles-for-use-in-mendeley/) to always output a DOI or PMID if available. An [example](nih.csl) NIH format based on the Nature format available in Mendeley is available in the repository. 

An example modified access macro is below:
```
  <macro name="access">
    <choose>
	 <if variable="PMCID">
        <text variable="PMCID" prefix="PMCID:"/>
      </if>
      <else-if variable="PMID">
        <text variable="PMID" prefix="PMID:"/>
      </else-if>
      <else-if variable="DOI">
        <text variable="DOI" prefix="doi:"/>
      </else-if>
      <else-if type="webpage" variable="URL" match="all">
        <text term="available at" text-case="capitalize-first" suffix=": "/>
        <text variable="URL" suffix=". "/>
        <group prefix="(" suffix=")" delimiter=": ">
          <text term="accessed" text-case="capitalize-first"/>
          <date variable="accessed">
            <date-part name="day" suffix=" " form="ordinal"/>
            <date-part name="month" suffix=" "/>
            <date-part name="year"/>
          </date>
        </group>
      </else-if>
    </choose>
  </macro>
```

As a note, the web API will send your DOIs or PMIDs over the internet to the NIH. If you prefer, you can download the latest PMCID database and perform the lookup offline.  This does, however, consume significant amounts of RAM (>2GB) to load.

Usage (Windows Binary)

* Download the [latest Windows binary](../../downloads/PMCIDConverter1.0.zip)
* Extract the zip file to a location on your computer
* Launch a [command prompt](http://lifehacker.com/5804483/open-hidden-menu-options-with-the-shift-and-right-click-shortcut)
* Invoke the executable with your appropriate arguments:
    * Use NIH Web API with example biblio:  
    PMCIDConverter.exe [biblio.txt](biblio.txt) biblio.out.txt  
or  
PMCIDConverter.exe [biblio.docx](biblio.docx) biblio.out.docx  

    * Use the NIH Downloaded database (not provided, see [https://www.ncbi.nlm.nih.gov/pmc/pmctopmid/])  
    PMCIDConverter.exe [biblio.txt](biblio.txt) biblio.out.txt  PMC-ids.csv  
or  
PMCIDConverter.exe [biblio.docx](biblio.docx) biblio.out.docx  PMC-ids.csv